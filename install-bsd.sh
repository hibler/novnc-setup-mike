#!/bin/sh

export ASSUME_ALWAYS_YES=true
sudo -E pkg install tigervnc-server autocutsel fvwm xorg-apps xorg-fonts xorg-fonts-100dpi xfontsel
if [ $? -ne 0 ]; then
    echo 'pkg install support failed'
    exit 1
fi

# use the default fvwm2 config like Linux does
if [ -r /usr/local/share/fvwm/default-config/config ]; then
    sudo cp -p /usr/local/share/fvwm/default-config/config /usr/local/etc/system.fvwm2rc
fi

exit 0

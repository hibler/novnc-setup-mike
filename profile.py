"""Setup VNC for a novnc demo

Instructions:
Wait for the experiment to start, click on the VNC option in the node
context menu.
"""

#
# This is the install/setup/start script.
#
STARTVNC = "/bin/sh /local/repository/startvnc.sh"

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as rspec
# Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Pick your OS.
imageList = [
    ('default', 'Default Image'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD', 'UBUNTU 18.04'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD', 'UBUNTU 20.04'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD', 'UBUNTU 16.04'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//CENTOS7-64-STD',  'CENTOS 7'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//CENTOS8-64-STD',  'CENTOS 8'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//FBSD114-64-STD', 'FreeBSD 11.4'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//FBSD124-64-STD', 'FreeBSD 12.3'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//FBSD132-64-STD', 'FreeBSD 13.0')]

pc.defineParameter("osImage", "Select OS for node running Xvnc",
                   portal.ParameterType.IMAGE,
                   imageList[2], imageList,
                   longDescription="Most clusters have this set of images, " +
                   "pick your favorite one.")
pc.defineParameter("nodeID", "Pick a specific node",
                   portal.ParameterType.STRING, "",
                   longDescription="If non-null, instantiate on the specific node specified")

# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()

#
# Declare that you will be starting X11 VNC on (some of) your nodes.
# You must have this line for X11 VNC to work.
#
request.initVNC()
 
# Add a raw PC to the request.
node = request.RawPC("node")
if params.osImage and params.osImage != "default":
    node.disk_image = params.osImage
if params.nodeID != "":
    node.component_id = params.nodeID

#
# Tell the system we are going to start X11 VNC on this node.
# Since the point of this profile is to demonstrate how, we
# indicate that we are starting it. Normally, this call would
# add whatever is needed to install and start VNC.
#
node.startVNC(nostart=True)

#
# Start X11 VNC from the repo.
# For now the port is 5901, at some point we may allow setting the port.
#
node.addService(rspec.Execute(shell="sh", command=STARTVNC))
        
portal.context.printRequestRSpec()
